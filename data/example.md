# Desmond Example

This will demonstrate an example of the Desmond tool.

## Alarm Setup

Set up the alarm information, for headings marked as alarm.

Note that the heading must be called `Alarm Setup`.

 * Alarm: Audio
 * Alarm-Repeat: 4
 * Alarm-Trigger: -PT5M
 * Alarm-Duration: PT1M15S

Other options are listed below (in a table as not to activate the
alarm detection.)

| Property       | Description                                         | Values                               |
|----------------|-----------------------------------------------------|--------------------------------------|
| Alarm          | Specifies which type of alarm to use                | `Audio`, `Email`, `Display` or `Off` |
| Alarm-Repeat   | How many times should the alarm repeat              | Unsigned integer                     |
| Alarm-Audio    | The uri of the audio to use for the alarm           | A Uri                                |
| Alarm-Atendee  | The atendees for the email alarm                    | An email adress                      |
| Alarm-Duration | The duration before the alarm repeats               | Written as a duration                |
| Alarm-Trigger  | How long before the start until the alarm goes off. | Written as a duration                |

### Note on durations

A duration is written as follows (defined in [RFC5455](https://datatracker.ietf.org/doc/html/rfc5545#section-3.3.6):
 * An optional `+` or `-` (a `-` should be used for triggers.)
 * A letter `P`.
 * The duration
     * A `W` for week.
     * A `D` for day, possible followed by a `T`:
     * If it is a time it is prefixed with a `T` then
         * A digit then a `H` for hour, possibly followed by an `M` value:
         * A digit then an `M` for minute, optionaly followed by an `S` value:
         * A digit then an `S` for second.

## Meeting with Jill <2021-08-01:12:30>

Remember, Jill can be particularly picky about her sandwiches,
must get jam, strawberry, not raspberry.

## Jill’s Birthday <2021-09-07> (1Y)

I am going to get Jill a piano, or at least a keyboard.
Grade 5 book too. This event will recur every year.

## This event happens three times <2021-09-08> (1D*3)

This is denoted by the fact that the repeat is written as `(1D*3)`,
symbolising three times.

## This happens till Nov <2021-09-08> (6D - <2021-11-01>)

This event will happen every 6 days until November. Shown by the
`- <2021-11-01>`, which means ‘until this date, repeat’

## No Thursday <2021-09-01> (1D [D MONDAY,TUESDAY,WED,FRI,SA,SU])

No one likes Thursdays. This is denoted by the
`[D MO,TU,WE,FR,SA,SU]`, where `D` specifies day, and the list
specifies *which* days. This can also be done with the following:

| Char code | Meaning        | Range                                               |
|-----------|----------------|-----------------------------------------------------|
| `s`       | by second      | (0 - 60)                                            |
| `m`       | by minute      | (0 - 59)                                            |
| `h`       | by hour        | (0 - 23)                                            |
| `W`       | by week number | (1 - 53) or (-1 - -53                               |
| `D`       | by day         | A day of the week at minimum 2 characters in length |
| `Md`      | by month day   | (1 - 31) or (-1 - -31)                              |
| `Yd`      | by year day    | (1 - 366) or (-1 - -366)                            |

## Daily routines

### ! Wake Up Alarm <2021-01-01:08:00> (1D)

Wake up, wake up!. This has an alarm, as indicated by the `!`.
The alarm data should be configured at the top of the file.

## Note:

This shall not be included in [Wake Up Alarm](#wake_up_alarm) as it
has a higher heading level.

## TODO: You can do todo’s

Just note that for now todos are not great, because they will never
reset no matter what repeats says. Basically they exist in the sense
that you can export them, nothing more.
