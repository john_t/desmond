pub use desmond;
use std::fs;
use structopt::StructOpt;
use std::path::PathBuf;

// Command line arguments
#[derive(Debug, StructOpt)]
pub struct Opt {
    /// Input file
    #[structopt(parse(from_os_str))]
    input: PathBuf,

    /// Output file, a change of the input if not present
    #[structopt(parse(from_os_str))]
    output: Option<PathBuf>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Parses the command line arguments.
    let mut opt = Opt::from_args();

    let desmond: desmond::Desmond =
        String::from_utf8(fs::read(&opt.input)?)?.parse()?;

    // Gets the calendar file
    opt.input.set_extension("ics");
    desmond.cal.save_file(opt.output.unwrap_or(opt.input))?;

    Ok(())
}
