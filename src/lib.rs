#![warn(clippy::pedantic)]

use chrono::prelude::*;
pub use ics::{Event, ICalendar, components::Component, ToDo};
use lazy_static::lazy_static;
use regex::Regex;
use std::str::FromStr;
use std::time::SystemTime;
use std::{fmt, mem};

mod alarm;
static DATEFORMAT: &str = "%Y%m%dT%H%M%SZ";

#[derive(Debug)]
pub enum DesmondError {
    ChronoParse(chrono::format::ParseError),
    InvalidTime,
    RUntilAndRCount,
    InvalidRFreq,
    InvalidRByDay,
    ParseAlarmRepeat(std::num::ParseIntError),
    AlarmError(alarm::Error),
}

impl fmt::Display for DesmondError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::ChronoParse(e) => write!(f, "{}", e),
            Self::InvalidTime => write!(f, "Invalid Time"),
            Self::RUntilAndRCount => {
                write!(f, "Repeat until and Repeat count specified")
            },
            Self::InvalidRFreq => write!(f, "Invalid freqency in repeat."),
            Self::InvalidRByDay => write!(f, "Invalid Day in repeating by"),
            Self::ParseAlarmRepeat(e) => {
                write!(f, "Error when parsing alarm repeat {}", e)
            },
            Self::AlarmError(e) => write!(f, "{}", e),
        }
    }
}

impl std::error::Error for DesmondError {}

#[derive(Debug, Clone)]
pub struct Desmond<'a> {
    pub cal: ICalendar<'a>,
}

impl Default for Desmond<'_> {
    fn default() -> Self {
        Self {
            cal: ICalendar::new("2.0", "desmond"),
        }
    }
}

fn apply_description<'a>(
    event: &mut Option<Component<'a>>,
    summary: &str,
    description: &mut Option<String>,
    apply_alarm: bool,
    alarm: Option<alarm::Template>,
    des: &mut Desmond<'a>,
) -> Result<(), alarm::Error> {
    if let Some(mut event) = mem::take(event) {
        if let Some(alarm) = alarm {
            if apply_alarm {
                event.add_component(alarm.into_alarm(
                    description.clone().unwrap_or_default(),
                    summary,
                )?);
            }
        }

        if let Some(description) = mem::take(description) {
            event.add_property(ics::properties::Description::new(ics::escape_text(
                description,
            )));
        }
        des.cal.add_component(event);
    }

    Ok(())
}

fn datetime(date: &str, time: Option<&str>) -> Result<String, DesmondError> {
    let dt;

    // Gets the date
    let date = Date::<Utc>::from_utc(
        NaiveDate::parse_from_str(date, "%Y-%m-%d")
            .map_err(DesmondError::ChronoParse)?,
        Utc,
    );
    if let Some(time) = time {
        let time = NaiveTime::parse_from_str(time, "%H:%M")
            .map_err(DesmondError::ChronoParse)?;
        let datetime = date.and_time(time).ok_or(DesmondError::InvalidTime)?;
        dt = datetime.format(DATEFORMAT).to_string();
    } else {
        dt = date.format("%Y%m%d").to_string();
    }
    Ok(dt)
}

fn calculate_heading_level(line: &str) -> Option<usize> {
    if line.as_bytes().get(0) == Some(&b'#') {
        let mut local_level = 0_usize;
        for byte in line.as_bytes() {
            if byte == &b'#' {
                local_level += 1;
            }
        }
        Some(local_level)
    } else {
        None
    }
}

fn get_recuring_data<'a>(
    captures: &regex::Captures,
) -> Result<Option<ics::properties::RRule<'a>>, DesmondError> {
    lazy_static! {
        static ref RFREQ_RE: Regex =
            Regex::new(r"(?P<n>\d+)(?P<scale>[smhDWMY])").unwrap();
        static ref RBY_NUM: Regex = Regex::new(r"\d+,+").unwrap();
        static ref RBY_DAY: Regex = Regex::new(
            r"(?xi)
            ^([
                (mo(n(day)?)?)
                (tu(e(s(day)?)?)?)
                (we(d(n(e(s(day)?)?)?)?)?)
                (th(u(r(s(day)?)?)?)?)
                (fr(i(day)?)?)
                (sa(t(u(r(day)?)?)?)?)
                (su(n(day)?)?)
            ],?)+$
        "
        )
        .unwrap();
    }

    // Repeats
    if let Some(rfreq) = captures.name("rfreq") {
        let rfreq = rfreq.as_str();
        let mut rrule = String::new();

        let rfreq = RFREQ_RE.captures(rfreq).unwrap();
        rrule.push_str(&format!(
            "FREQ={};INTERVAL={};",
            match rfreq.name("scale").unwrap().as_str() {
                "s" => "SECONDLY",
                "m" => "MINUTELY",
                "h" => "HOURLY",
                "D" => "DAILY",
                "W" => "WEEKLY",
                "M" => "MONTHLY",
                "Y" => "YEARLY",
                _ => return Err(DesmondError::InvalidRFreq),
            },
            rfreq.name("n").unwrap().as_str(),
        ));

        match (captures.name("runtil_date"), captures.name("rcount")) {
            // No limit
            (None, None) => (),
            // Goes until a date
            (Some(until), None) => rrule.push_str(&format!(
                "UNTIL={};",
                datetime(
                    until.as_str(),
                    captures.name("runtil_time").map(|x| x.as_str()),
                )?,
            )),
            // Goes until a count
            (None, Some(count)) => {
                rrule.push_str(&format!("COUNT={};", count.as_str()));
            },
            // Invalid!
            (Some(_), Some(_)) => return Err(DesmondError::RUntilAndRCount),
        }

        // By
        if let Some(rby) = captures.name("rby") {
            if let Some(rby_scale) = captures.name("rby_scale") {
                let rby = rby.as_str();
                let rby_scale = rby_scale.as_str();
                match rby_scale {
                    // These are just numbers
                    "s" | "m" | "h" | "W" | "Md" | "Yd" | "M" => {
                        rrule.push_str(match rby_scale {
                            "s" => "BYSECOND",
                            "m" => "BYMINUTE",
                            "h" => "BYHOUR",
                            "W" => "BYWEEKNO",
                            "Md" => "BYMONTHDAY",
                            "Yd" => "BYYEARDAY",
                            _ => panic!("Messed up logic"),
                        });
                        rrule.push('=');
                        rrule.push_str(rby);
                    },
                    "D" => {
                        if RBY_DAY.is_match(rby) {
                            rrule.push_str("BYDAY=");
                            for split in rby.split(',').map(|x: &str| &x[..2]) {
                                rrule.push_str(split);
                                rrule.push(',');
                            }
                            rrule.pop();
                        } else {
                            return Err(DesmondError::InvalidRByDay);
                        }
                    },
                    _ => return Err(DesmondError::InvalidRFreq),
                }
                rrule.push(';');
            }
        }

        rrule.pop();
        Ok(Some(ics::properties::RRule::new(rrule)))
    } else {
        Ok(None)
    }
}

fn create_event<'a, 's>(
    line: &'s str,
    summary: &mut &'s str,
) -> Result<Option<(Component<'a>, bool)>, DesmondError> {
    lazy_static! {
       static ref RE: Regex = Regex::new(
           r"(?x)

            # Title
            ^\#+

            \s*

            # Alarm
            (?P<alarm>!)?

            \s*

            # ToDo
            (?P<todo>([Tt][Oo][Dd][Oo]|[Dd][Oo][Nn][Ee]))?:?

            \s*

            # Summary
            (?P<summary>[^<>]+)

            # Date
            (<(?P<date>(\d{4}-\d{2}-\d{2}))(:(?P<time>\d{2}:\d{2}))?>)?
            \s*

            (\(
            \s*

            # Repeat Frequency
            ((?P<rfreq>(\d+[smhDWMY])))?
            \s*

            # Repeat Until
            (-\s*<(?P<runtil_date>(\d{4}-\d{2}-\d{2}))(:(?P<runtil_time>\d{2}:\d{2}))?>)?

            # Repeat Count
            (\*(?P<rcount>((\d+))))?

            # BY
            (\[
                (?P<rby_scale>[smhDWM(Md)(Yd)])
                \s*
                (?P<rby>([\+-]?\w+?,?)+)
            \])?
            \s*
            \))?
            
        "
       )
       .unwrap();

    }
    if let Some(captures) = RE.captures(line) {
        let mut component: Component;

        // Creates the date time object
        let dt;
        if let Some(date) = captures.name("date") {
            dt = Some(datetime(
                date.as_str(),
                captures.name("time").map(|x| x.as_str()),
            ));
        } else {
            dt = None;
        }

        let apply_alarm = captures.name("alarm").is_some();

        if let Some(todo_str) = captures.name("todo") {

            // Creates an event
            let mut todo =
                ToDo::new(
                    uuid::Uuid::new_v4().to_string(),
                    DateTime::<Utc>::from(SystemTime::now()).format(DATEFORMAT).to_string(),
                );

            // Add the start time
            if let Some(dt) = dt {
                let dt = dt?;
                todo.push(ics::properties::Due::new(dt.clone()));
                todo.push(ics::properties::DtStart::new(dt));
            }

            if todo_str.as_str().to_lowercase() == "DONE" {
                todo.push(ics::properties::Completed::new(
                    DateTime::<Utc>::from(SystemTime::now()).format(DATEFORMAT).to_string()
                ));
            }

            component = todo.into();
        } else {

            // Creates an event
            let mut event =
                Event::new(
                    uuid::Uuid::new_v4().to_string(),
                    DateTime::<Utc>::from(SystemTime::now()).format(DATEFORMAT).to_string(),
                );

            // Add the start time
            if let Some(dt) = dt {
                event.push(ics::properties::DtStart::new(dt?));
            } else {
                return Ok(None);
            }

            component = event.into();
        }

        // Give it a summary
        *summary = captures.name("summary").unwrap().as_str();
        component.add_property(ics::properties::Summary::new(summary.to_owned()));

        // Add information about repeating events
        if let Some(rrule) = get_recuring_data(&captures)? {
            component.add_property(rrule);
        }

        Ok(Some((component, apply_alarm)))
    } else {
        Ok(None)
    }
}

impl FromStr for Desmond<'_> {
    type Err = DesmondError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE_ALARM_HEADING: Regex = Regex::new(
                r#"(?xi)
                ^\#+\s*Alarm\s*Setup
            "#
            )
            .unwrap();
        }
        let mut des = Desmond::default();

        // Split the string into lines, as markdown is mainly line based.
        let split = s.replace('\r', "");
        let split = split.split('\n');

        let mut event: Option<Component> = None;
        let mut description: Option<String> = None;
        let mut level = 0;
        let mut alarm_setup = false;
        let mut alarm = None;
        let mut apply_alarm = false;
        let mut summary = "";

        // Loop through the split
        for i in split {
            // Compute the level
            if let Some(local_level) = calculate_heading_level(i) {
                if local_level <= level {
                    // If the local level is smaller than the large
                    // level then this means we need to apply the
                    // description and suchlike.
                    alarm_setup = false;
                    apply_description(
                        &mut event,
                        summary,
                        &mut description,
                        apply_alarm,
                        alarm.clone(),
                        &mut des,
                    )?;
                }
                level = local_level;
            }

            // See if we can get a match
            if let Some((local_event, local_apply_alarm)) =
                create_event(i, &mut summary)?
            {
                alarm_setup = false;
                // Adds the description to the previous event.
                apply_description(
                    &mut event,
                    summary,
                    &mut description,
                    apply_alarm,
                    alarm.clone(),
                    &mut des,
                )?;
                apply_alarm = local_apply_alarm;

                event = Some(local_event);
            } else if RE_ALARM_HEADING.is_match(i) {
                // Indicate that wer are in an alarm section, and so
                // we should watch out for alarm properties.
                alarm_setup = true;
            } else {
                // Checks if we are at a heading, if so we don't
                // need to continue setting up the alarm.
                if i.as_bytes().get(0) == Some(&b'#') {
                    alarm_setup = false;
                } else if alarm_setup {
                    alarm::apply_alarm_properties(i, &mut alarm)?;
                }

                // Append to the description
                if description.is_none() && event.is_some() {
                    description = Some(String::new());
                }
                if let Some(ref mut description) = description {
                    description.push_str(i);
                    description.push('\n');
                }
            }
        }

        // Add the description to the event.
        apply_description(
            &mut event,
            summary,
            &mut description,
            apply_alarm,
            alarm,
            &mut des,
        )?;

        Ok(des)
    }
}
