use super::DesmondError;
use ics::Alarm;
use lazy_static::lazy_static;
use regex::Regex;
use std::fmt;

#[derive(Debug)]
pub enum Error {
    InvalidDuration,
    InvalidAlarmProp,
    NoAlarmType,
    NoTrigger,
}

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::InvalidDuration => {
                write!(f, "Invalid Duration whilst parsing alarm.")
            },
            Self::InvalidAlarmProp => write!(f, "Invalid Alarm Property."),
            Self::NoAlarmType => write!(f, "No alarm type"),
            Self::NoTrigger => write!(f, "No trigger in alarm"),
        }
    }
}

impl From<Error> for DesmondError {
    fn from(ae: Error) -> Self {
        Self::AlarmError(ae)
    }
}

#[derive(Clone, Debug)]
pub enum Action {
    Email,
    Audio,
    Display,
}

#[derive(Default, Clone, Debug)]
pub(super) struct Template {
    pub action: Option<Action>,
    pub repeat: Option<u16>,
    pub audio: Option<String>,
    pub attendee: Option<String>,
    pub duration: Option<String>,
    pub trigger: Option<String>,
}

impl Template {
    pub fn into_alarm<'a>(
        self,
        description: String,
        summary: &str,
    ) -> Result<Alarm<'a>, Error> {
        use ics::properties::{
            Attach, Attendee, Description, Duration, Repeat, Summary, Trigger,
        };
        let mut alarm = match self.action.ok_or(Error::NoAlarmType)? {
            Action::Audio => {
                let mut alarm = Alarm::audio(Trigger::new(
                    self.trigger.ok_or(Error::NoTrigger)?,
                ));
                if let Some(audio) = self.audio {
                    alarm.push(Attach::new(audio));
                }
                alarm
            },
            Action::Display => Alarm::display(
                Trigger::new(self.trigger.ok_or(Error::NoTrigger)?),
                Description::new(format!(
                    "{}\n\n{}",
                    ics::escape_text(summary),
                    ics::escape_text(description)
                )),
            ),
            Action::Email => {
                let mut alarm = Alarm::email(
                    Trigger::new(self.trigger.ok_or(Error::NoTrigger)?),
                    Description::new(ics::escape_text(description)),
                    Summary::new(ics::escape_text(summary.to_owned())),
                );

                // You can actually send things in emails.
                if let Some(audio) = self.audio {
                    alarm.push(Attach::new(audio));
                }

                // Who are we sending the email too
                if let Some(attendee) = self.attendee {
                    alarm.push(Attendee::new(format!(
                        "mailto:{}",
                        ics::escape_text(attendee)
                    )));
                }
                alarm
            },
        };

        // Adds the duration
        if let Some(duration) = self.duration {
            alarm.push(Duration::new(duration));
        }

        // Adds the repeat
        if let Some(repeat) = self.repeat {
            alarm.push(Repeat::new(repeat.to_string()));
        }

        Ok(alarm)
    }
}

/// This should be run whilst in an alarm configuration block when
/// parsing a `Desmond`.
pub(crate) fn apply_alarm_properties(
    line: &str,
    alarm: &mut Option<Template>,
) -> Result<(), DesmondError> {
    lazy_static! {
        static ref RE_ALARM_PROPERTY: Regex = Regex::new(
            r#"(?xi)
            # Matches against bullet, either written correctly
            # * as so,
            # - or incorrectly, with a dash.
            ^\s*[*-]\s
            
            # Matches a property:
            # Alarm
            # Alarm-Repat
            # Alarm-Audio
            # Alarm-Attendee
            # Alarm-Duration
            # Alarm-Trigger
            (?P<prop>(Alarm(-(Repeat|Audio|Attendee|Duration|Trigger))?))

            # Matches the inbetween
            \s*:\s*

            # Matches the value
            (?P<val>(.+))$
        "#
        )
        .unwrap();
        static ref RE_DURATION: Regex = Regex::new(
            r#"(?ix)
            [\+-]?P(\d+W|\d+D|(\d+D)?T(\d+H(\d+M(\d+S)?)?|\d+M(\d+S)?)|\d+S)
        "#
        )
        .unwrap();
    };

    // Checks if this line is an alarm property line.
    if let Some(alarm_prop) = RE_ALARM_PROPERTY.captures(line) {
        // Creates an alarm template if it doesn't already
        // exist.
        if alarm.is_none() {
            *alarm = Some(Template::default());
        }
        let u_alarm = alarm.as_mut().unwrap();

        // Matches the property which we have recieved
        match (
            alarm_prop
                .name("prop")
                .unwrap()
                .as_str()
                .to_uppercase()
                .as_str(),
            alarm_prop.name("val").unwrap().as_str(),
        ) {
            ("ALARM", "Audio") => {
                u_alarm.action = Some(Action::Audio);
            },
            ("ALARM", "Email") => {
                u_alarm.action = Some(Action::Email);
            },
            ("ALARM", "Display") => {
                u_alarm.action = Some(Action::Display);
            },
            ("ALARM", "Off") => *alarm = None,
            ("ALARM-REPEAT", int) => {
                u_alarm.repeat =
                    Some(int.parse().map_err(DesmondError::ParseAlarmRepeat)?);
            },
            ("ALARM-AUDIO", audio) => {
                u_alarm.audio = Some(audio.to_owned());
            },
            ("ALARM-ATENDEE", attendee) => {
                u_alarm.attendee = Some(attendee.to_owned());
            },
            ("ALARM-DURATION", duration) => {
                if RE_DURATION.is_match(duration) {
                    u_alarm.duration = Some(duration.to_owned());
                } else {
                    return Err(Error::InvalidDuration.into());
                }
            },
            ("ALARM-TRIGGER", trigger) => {
                if RE_DURATION.is_match(trigger) {
                    u_alarm.trigger = Some(trigger.to_owned());
                } else {
                    return Err(Error::InvalidDuration.into());
                }
            },
            (_, _) => return Err(Error::InvalidAlarmProp.into()),
        }
    }

    Ok(())
}
