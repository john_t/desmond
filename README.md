# Desmond

Desmond is a program to convert markdown files into calendar files using a simple and readable syntax. A bit like org mode from emacs, but without the emacs part.

Check out the [example file](data/example.md)
